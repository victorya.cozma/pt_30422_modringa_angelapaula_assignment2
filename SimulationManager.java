import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;


public class SimulationManager implements Runnable{

	private int numberOfClients;
	private int numberOfServers;
	private int simulationTimeInterval;
	private int minArrivalTime;
	private int maxArrivalTime;
	private int maxProcessingTime;
	private int minProcessingTime;
	
	private Scheduler scheduler;
	private List<Client> generatedClients;
	
	FileWriter outputFileWriter;
	
	public SimulationManager(String inputFileName, String output) {
		try {
			this.outputFileWriter = new FileWriter(output);
		}catch(IOException e) {
			System.out.println(e.getMessage());
		}
		try {
			File inputFile = new File(inputFileName);
			BufferedReader myReader = new BufferedReader(new FileReader(inputFile));
	    	this.numberOfClients = Integer.parseInt(myReader.readLine());
	    	this.numberOfServers = Integer.parseInt(myReader.readLine());
	    	this.simulationTimeInterval = Integer.parseInt(myReader.readLine());
	    	String arrival = new String();
	    	String processing = new String();
	    	arrival = myReader.readLine();
	    	processing = myReader.readLine();
	    	String timesA[] = arrival.split(",");
	    	this.minArrivalTime = Integer.parseInt(timesA[0]);
	    	this.maxArrivalTime = Integer.parseInt(timesA[1]);
	    	String timesP[] = processing.split(",");
	    	this.minProcessingTime = Integer.parseInt(timesP[0]);
	    	this.maxProcessingTime = Integer.parseInt(timesP[1]);
	    	myReader.close();
		}catch(FileNotFoundException e){
			System.out.println("Error!File not found");	
		}catch(IOException e) {
			System.out.println(e.getMessage());
		}
		this.generateNRandomClients();
		this.scheduler = new Scheduler(this.numberOfServers);
	}
	
	public void setClientsList(List<Client> obtainedList) {
		int id = 1;
		for(Client c: obtainedList) {
			c.setClientId(id);
			id++;
		}
		this.generatedClients = obtainedList;
	}
	
	public void generateNRandomClients() {
	
		List<Client> clientsGenerated = new ArrayList<Client>();
		
		for(int i = 0 ; i < numberOfClients ; i++) {
			int arrivalTime = (int)((Math.random() * (maxArrivalTime - minArrivalTime + 1)) + minArrivalTime);
			int processingTime = (int)((Math.random() * (maxProcessingTime- minProcessingTime + 1)) + minProcessingTime);
			Client newClient = new Client(arrivalTime, processingTime);
			clientsGenerated.add(newClient);
		}
		
		Collections.sort(clientsGenerated);
		this.setClientsList(clientsGenerated);
	}
	
	public List<Client> getGeneratedClients() {
		return this.generatedClients;
	}
	
	@Override
	public void run(){
		int currentSimulationTime = 0;
		while(currentSimulationTime <= simulationTimeInterval) {
			int noQueuesClosed = 0;
			Iterator<Client> iterator = this.generatedClients.iterator();
			while(iterator.hasNext()) {
				Client client = (Client) iterator.next();
				if(client.getArrivalTime() == currentSimulationTime) {
					this.scheduler.dispatchClient(client);
					iterator.remove(); 
				}
			}
			try {
				outputFileWriter.write("\nTime:" + currentSimulationTime + "\n" + "Waiting clients: ");
				for (Client c : generatedClients)
					outputFileWriter.write(("(" + c.getId() + "," + c.getArrivalTime() + "," + c.getProcessingTime() + "); "));
				outputFileWriter.write("\n");
				for(Server s : scheduler.getServers()) {
					outputFileWriter.write("Queue " + s.getServerId() + ":");
					if(s.getClients().isEmpty())
						outputFileWriter.write("closed");
					else 
						for(Client c : s.getClients())
							outputFileWriter.write("(" + c.getId() + "," + c.getArrivalTime() + "," + c.getProcessingTime() + "); ");
					outputFileWriter.write("\n");
				}
			}catch(IOException e) {}
			for (Server s : scheduler.getServers())
				if (s.getClients().isEmpty())
					noQueuesClosed++;		
			if (((generatedClients.isEmpty()) && (scheduler.getServers().size() == noQueuesClosed)) || simulationTimeInterval == currentSimulationTime) {
				scheduler.setTotalWaitingTime();
				float averageWaitingTime = scheduler.getTotalWaitingTime()*1.0f / numberOfClients;
				try {
					outputFileWriter.write("\nAverage waiting time: " + averageWaitingTime);
					outputFileWriter.flush();
				}
				catch(IOException e) {}
				System.out.println("done!");
				System.exit(0);
			}
			currentSimulationTime++;
			try {
				Thread.sleep(1000);
			}catch (InterruptedException e) {}
			try {
			outputFileWriter.flush();
			}catch(IOException e) {}
		} 
		try {
			outputFileWriter.close();
		}
		catch(IOException e) {}
	}
	
	public static void main(String[] args) {

		SimulationManager manager = new SimulationManager(args[0], args[1]);
		Thread t = new Thread(manager);
		t.start();
		
	}
	
}
